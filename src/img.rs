// Custom headers for thumbnail parameters
header! { (XWidth, "X-Width") => [u32] }
header! { (XHeight, "X-Height") => [u32] }
header! { (XOpacity, "X-Opacity") => [u8] }


use image;
use image::{FilterType, ImageFormat, GenericImage, DynamicImage, Pixel};

use iron::prelude::*;
use iron::headers::Headers;
use iron::status;

pub fn convert(buf: &mut Vec<u8>, format: ImageFormat, hdrs: Headers) -> IronResult<()> {

    // Load image from source buffer
    let mut img = match image::load_from_memory_with_format(buf, format) {
        Ok(v) => v,
        Err(e) => return Err(IronError::new(e, status::InternalServerError)),
    };

    // Get Dimensions
    let (src_width, src_height) = (&img).dimensions();

    // Load image size and opacity parameters from headers
    let mut width: u32 = match hdrs.get::<XWidth>() {
        Some(v) => **v,
        None => src_width,
    };
    let mut height: u32 = match hdrs.get::<XHeight>() {
        Some(v) => **v,
        None => src_height,
    };
    let opacity: u8 = match hdrs.get::<XOpacity>() {
        Some(v) => **v,
        None => 255,
    };

    // Cap width and height to prevent super sampling
    if width > src_width {
        width = src_width;
    }

    if height > src_height {
        height = src_height;
    }
    // resize the image
    img = img.resize(width, height, FilterType::Nearest);

    // Apply opacity if need be
    if opacity < 255 {
        let mut imgbuf = img.to_rgba();
        for p in imgbuf.pixels_mut() {
            let rgba = p.channels_mut();
            rgba[3] = opacity
        }

        img = DynamicImage::ImageRgba8(imgbuf);
    }
    // Save image to output buffer
    buf.truncate(0);
    match img.save(buf, image::PNG) {
        Ok(_) => Ok(()),
        Err(e) => Err(IronError::new(e, status::InternalServerError)),
    }
}
