#[macro_use]
extern crate hyper;
#[macro_use]
extern crate iron;
#[macro_use]
extern crate router;
extern crate image;

use image::ImageFormat;

use iron::prelude::*;
use iron::status;
use iron::headers::{ContentType, ContentLength};
use iron::mime::{Mime, TopLevel, SubLevel};

use std::io::Read;

mod img;
mod pdf;

const MAX_BODY_SIZE: u64 = 10 * 1024 * 1024; //10MB

fn main() {
    let router = router!(get "/" => index,
        put "/" => convert,
        post "/" => convert);

    Iron::new(router)
        .http("localhost:8080")
        .unwrap();
}

fn index(_: &mut Request) -> IronResult<Response> {
    let mut res = Response::new();
    res.status = Some(status::Ok);
    res.headers.set(ContentType::html());

    res.body = Some(Box::new("<html> <body> <h1>Welcome to the Thumbnail server.</h1> \
                              <p>Supported mime types are: <ul> <li>application/pdf</li> \
                              <li>image/jpeg</li> <li>image/png</li> <li>image/gif</li> \
                              <li>image/tiff</li> </ul> </p> </body> </html>"));

    Ok(res)
}

fn convert(req: &mut Request) -> IronResult<Response> {
    let len: u64 = match req.headers.get::<ContentLength>() {
        Some(v) => **v,
        None => return Ok(Response::with((status::BadRequest, "no content provided"))),
    };

    // Max upload size is 10MB
    if len > MAX_BODY_SIZE {
        return Ok(Response::with((status::PayloadTooLarge, "content too large")));
    }

    // Get string version of ContentType
    let ctype: &Mime = match req.headers.get::<ContentType>() {
        Some(v) => v,
        None => return Ok(Response::with((status::BadRequest, "no content type"))),
    };

    // Create buffer
    let mut buf: Vec<u8> = Vec::with_capacity(len as usize);

    // Read into buffer
    itry!(req.body.read_to_end(&mut buf));

    // Create the Thumbnail based on mime type
    if *ctype ==
       Mime(TopLevel::Application,
            SubLevel::Ext(String::from("pdf")),
            vec![]) {
        try!(pdf::convert(&mut buf, req.headers.clone()));
    } else {
        let format: ImageFormat = match ctype.1 {
            SubLevel::Gif => ImageFormat::GIF,
            SubLevel::Jpeg => ImageFormat::JPEG,
            SubLevel::Png => ImageFormat::PNG,
            _ => {
                // Exit early as the image type is not supported
                if ctype.1 != SubLevel::Ext(String::from("tiff")) {
                    return Ok(Response::with((status::BadRequest, "unsupported type")));
                }
                ImageFormat::TIFF
            }
        };

        try!(img::convert(&mut buf, format, req.headers.clone()));
    }

    // Thumbnail created now return to caller
    let mut res = Response::new();
    res.status = Some(status::Ok);
    res.headers.set(ContentType(Mime(TopLevel::Image, SubLevel::Png, vec![])));
    res.headers.set(ContentLength(buf.len() as u64));

    res.body = Some(Box::new(buf));
    Ok(res)
}
