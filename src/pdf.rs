use iron::headers::Headers;

use iron::prelude::*;
use iron::status;


use std::io::{Read, Write, Error, ErrorKind};
use std::process::{Command, Stdio};

pub fn convert<'a>(buf: &mut Vec<u8>, hdrs: Headers) -> IronResult<()> {
    let hdr_prefix: &'a str = "X-Imagemagick";

    // Build up commandline arguments
    let mut args: Vec<String> = hdrs.iter()
        .filter(|h| h.name().starts_with(hdr_prefix))
        .flat_map(|h| vec![String::from(h.name().trim_left_matches(hdr_prefix)), h.value_string()])
        .collect();

    // Add commandline arguments for the input and output of the command
    args.insert(0, String::from("pdf:-[0]"));
    args.push(String::from("png:-"));

    let mut child = itry!(Command::new("convert")
        .args(&args)
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn());

    // Scope the code below to make sure the child_in borrow ends
    // before we need access to child again.
    {
        let child_in = match child.stdin.as_mut() {
            Some(v) => v,
            None => {
                return Err(IronError::new(Error::new(ErrorKind::BrokenPipe, "no stdin"),
                                          status::InternalServerError))
            }
        };
        itry!(child_in.write(buf));
    }
    // Drop stdin to prevent process lockup
    drop(child.stdin.take());

    // Read the stdout to get the results of the convert command
    {
        let child_out = match child.stdout.as_mut() {
            Some(v) => v,
            None => {
                return Err(IronError::new(Error::new(ErrorKind::BrokenPipe, "no stdout"),
                                          status::InternalServerError))
            }
        };
        buf.truncate(0);
        itry!(child_out.read_to_end(buf));
    }
    // Wait and confirm process has exited
    itry!(child.wait());
    Ok(())
}
