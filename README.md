# Thumbnail Server

This is a simple micro service for creating thumbnails from larger images as well
as the front pages of PDFs.

This is not meant to be exposed externally as there are no authentication or
security safe guards.

The service utilizes ImageMagick convert to generate the thumbnails. It is most
ideal to run the application from within a pre packaged container.
